#!/bin/bash
#
# Name:     gradle-check
# Author:   mrt181@gmail.com
#
# check project before commit
# if check fails, do not commit
# stash changes prior to check

STASH_NAME="pre-commit-$(date +%s)"
git stash save -q --keep-index $STASH_NAME
./gradlew clean check > /dev/null 2>&1
result=$?
if [ $result -ne 0 ]; then
    STASHES=$(git stash list)
    if [[ $STASHES == "$STASH_NAME" ]]; then
        git stash pop -q
    fi
    echo $result
    exit 1
fi

STASHES=$(git stash list)
if [[ $STASHES == "$STASH_NAME" ]]; then
  git stash pop -q
fi
